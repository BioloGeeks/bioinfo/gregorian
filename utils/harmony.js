#! /usr/bin/env node
/**
 * Generate an mapping between DNA k-mer and a tone
 */

import product from './product.js';
import fs from 'fs';

let DNA = ["A", "C", "G", "T"];

let tones = ["C", "D", "E", "F", "G", "A", "B"];

let duration = {
    "noire": 0.5,
    "blanche": 1,
    "ronde": 2,
    "croche": 0.25,
    "double croche": 0.125,
};

let k = 4;

// Generate all possible k-mers
let kmers = product('ACGT'.split(''), k);

// Generate a random mapping between k-mers and tones
let mapping = kmers.map(kmer => {
    let tone = tones[Math.floor(Math.random() * tones.length)];
    let dur = Object.keys(duration)[Math.floor(Math.random() * Object.keys(duration).length)];
    return {
        kmer: kmer.join(''),
        tone: tone,
        duration: dur
    };
});


const json = JSON.stringify(mapping, null, 2);
fs.writeFileSync('src/assets/js/tune/mapping.json', json);