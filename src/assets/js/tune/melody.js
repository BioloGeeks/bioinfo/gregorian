import { play } from './sound.js';
import mapping from './mapping.json';
import notes from './notes-frequencies.json';
import durations from './duration.json';
import choice from '../../../../utils/choice.js';

const k = 4;
let interval = undefined;
let time = 0;
let melody = [];

function playNote(note) {
    let delay = durations[choice(mapping.map(note => note.duration))];
    let frequency = notes[`${note.tone}4`];
    play(delay, frequency, durations[note.duration]);
}

function playMelody(sequence) {
    melody = [];
    for (let i = 0; i < sequence.length - k; i++) {
        let kmer = sequence.substring(i, i + k);
        let note = mapping.find(note => note.kmer === kmer);
        if (note) {
            melody.push(note);
        }
    }
    time = 0;
    resumeMelody();
}

function pauseMelody() {
    window.clearInterval(interval);
}

function resumeMelody() {
    interval = window.setInterval(() => {
        if (time < melody.length) {
            playNote(melody[time]);
        }
        time++;
    }, 500); // TODO set customizable tempo   
}

export {
    playMelody,
    resumeMelody,
    pauseMelody
};