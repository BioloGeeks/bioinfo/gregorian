let context = undefined;

function play(delay, frequency, duration) {
    if (!context) {
        context = new AudioContext();
    }
    const oscillator = context.createOscillator();
    oscillator.frequency.value = frequency;
    const waveForm = Math.random() > 0.5 ? "sine" : "triangle";
    oscillator.type = waveForm;
    const gainNode = context.createGain();
    gainNode.gain.value = .2;

    // generate sound
    oscillator.connect(gainNode);
    gainNode.connect(context.destination);

    oscillator.start(0);

    gainNode.gain.linearRampToValueAtTime(0.0001, context.currentTime + duration);
    oscillator.stop(context.currentTime + duration);
}

export {
    play
};