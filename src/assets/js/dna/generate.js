import choice from "../../../../utils/choice.js";

const nucleotides = "ACGT";

function generateDNA(length) {
  let dna = "";
  for (let i = 0; i < length; i++) {
    dna += choice(nucleotides);
  }
  return dna;
}

export default generateDNA;