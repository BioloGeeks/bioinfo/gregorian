# Gregorian

Listen to DNA sequences.

## Introduction

Split a dna sequence into k-mers and associate for each k-mer a note. Generate a tape and listen to it.

## Installation

### Requirements

- Git
- NodeJS and Yarn (for static site build)

### Process

Clone repository
```bash
git clone https://framagit.org/BioloGeeks/bioinfo/gregorian.git
cd gregorian
```

Install dependencies
```bash
yarn Install
```

Generate static site
```bash
yarn build
```

## Usage

Launch a local server
```bash
yarn start
```

## Reference

[1] [Mendel](https://github.com/ekimb/mendel)